import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./auth/auth.guard";
import { AnnonceListComponent } from "./annonces/annonce-list/annonce-list.component";
import { AnnonceCreateComponent } from "./annonces/annonce-create/annonce-create.component";

const routes: Routes = [
  { path: "", component: AnnonceListComponent },
  { path: "create", component: AnnonceCreateComponent, canActivate: [AuthGuard] },
  { path: "edit/:annonceId", component: AnnonceCreateComponent, canActivate: [AuthGuard] },
  { path: "auth", loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule {}
