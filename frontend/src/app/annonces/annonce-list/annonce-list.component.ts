import { Component, OnDestroy, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { Annonce } from '../annonce.model';
import { AnnoncesService } from '../annonces.service';

@Component({
  selector: 'app-annonce-list',
  templateUrl: './annonce-list.component.html',
  styleUrls: ['./annonce-list.component.scss']
})
export class AnnonceListComponent implements OnInit, OnDestroy {
  annonces: Annonce[] = [];
  isLoading = false;
  totalAnnonces = 0;
  annoncesPerPage = 5;
  currentPage = 1;
  pageSizeOptions = [1, 2, 5, 10];
  userIsAuthenticated = false;
  userId: string;
  private annoncesSub: Subscription;
  private authStatusSub: Subscription;
  displayedColumns: string[] = ['id', 'image', 'titre', 'action'];
  dataSource: MatTableDataSource<Annonce>;

  constructor(
    public annoncesService: AnnoncesService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.isLoading = true;
    this.annoncesService.getAnnonces(this.annoncesPerPage, this.currentPage);
    this.userId = this.authService.getUserId();
    this.annoncesSub = this.annoncesService
      .getAnnonceUpdateListener()
      .subscribe((annonceData: { annonces: Annonce[]; annonceCount: number }) => {
        this.isLoading = false;
        this.totalAnnonces = annonceData.annonceCount;
        this.annonces = annonceData.annonces;
        this.dataSource = new MatTableDataSource(annonceData.annonces);
      });
    this.userIsAuthenticated = this.authService.getIsAuth();
    this.authStatusSub = this.authService
      .getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated;
        this.userId = this.authService.getUserId();
      });
  }

  onChangedPage(pageData: PageEvent) {
    this.isLoading = true;
    this.currentPage = pageData.pageIndex + 1;
    this.annoncesPerPage = pageData.pageSize;
    this.annoncesService.getAnnonces(this.annoncesPerPage, this.currentPage);
  }

  onDelete(annonceId: string) {
    this.isLoading = true;
    this.annoncesService.deleteAnnonce(annonceId).subscribe(() => {
      this.annoncesService.getAnnonces(this.annoncesPerPage, this.currentPage);
    }, () => {
      this.isLoading = false;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnDestroy() {
    this.annoncesSub.unsubscribe();
    this.authStatusSub.unsubscribe();
  }

}
