export interface Annonce {
  id: string;
  title: string;
  description: string;
  image: string;
  creator: string;
}
