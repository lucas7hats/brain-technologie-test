import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { Annonce } from '../annonce.model';
import { AnnoncesService } from '../annonces.service';
import { mimeType } from './mime-type.validator';

@Component({
  selector: 'app-annonce-create',
  templateUrl: './annonce-create.component.html',
  styleUrls: ['./annonce-create.component.scss']
})
export class AnnonceCreateComponent implements OnInit {
  enteredTitle = "";
  enteredContent = "";
  annonce: Annonce;
  isLoading = false;
  form: FormGroup;
  imagePreview: string;
  private mode = "create";
  private annonceId: string;
  private authStatusSub: Subscription;

  constructor(
    public annoncesService: AnnoncesService,
    public route: ActivatedRoute,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.authStatusSub = this.authService
      .getAuthStatusListener()
      .subscribe(authStatus => {
        this.isLoading = false;
      });
    this.form = new FormGroup({
      title: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(3)]
      }),
      description: new FormControl(null, { validators: [Validators.required] }),
      image: new FormControl(null, {
        validators: [Validators.required],
        asyncValidators: [mimeType]
      })
    });
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has("annonceId")) {
        this.mode = "edit";
        this.annonceId = paramMap.get("annonceId");
        this.isLoading = true;
        this.annoncesService.getAnnonce(this.annonceId).subscribe(annonceData => {
          this.isLoading = false;
          this.annonce = {
            id: annonceData._id,
            title: annonceData.title,
            description: annonceData.description,
            image: annonceData.image,
            creator: annonceData.creator
          };
          this.form.setValue({
            title: this.annonce.title,
            description: this.annonce.description,
            image: this.annonce.image
          });
        });
      } else {
        this.mode = "create";
        this.annonceId = null;
      }
    });
  }

  onImagePicked(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.form.patchValue({ image: file });
    this.form.get("image").updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result as string;
    };
    reader.readAsDataURL(file);
  }

  onSaveAnnonce() {
    if (this.form.invalid) {
      return;
    }
    this.isLoading = true;
    if (this.mode === "create") {
      this.annoncesService.addAnnonce(
        this.form.value.title,
        this.form.value.description,
        this.form.value.image
      );
    } else {
      this.annoncesService.updateAnnonce(
        this.annonceId,
        this.form.value.title,
        this.form.value.description,
        this.form.value.image
      );
    }
    this.form.reset();
  }

  ngOnDestroy() {
    this.authStatusSub.unsubscribe();
  }

}
