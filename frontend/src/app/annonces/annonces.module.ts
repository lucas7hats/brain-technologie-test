import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnnonceCreateComponent } from './annonce-create/annonce-create.component';
import { AnnonceListComponent } from './annonce-list/annonce-list.component';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [AnnonceCreateComponent, AnnonceListComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    RouterModule
  ]
})
export class AnnoncesModule { }
