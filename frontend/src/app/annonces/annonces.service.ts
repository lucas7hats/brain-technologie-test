import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { map } from "rxjs/operators";
import { environment } from 'src/environments/environment';
import { Annonce } from './annonce.model';

const BACKEND_URL = environment.apiUrl + "/annonces/";

@Injectable({
  providedIn: 'root'
})
export class AnnoncesService {
  private annonces: Annonce[] = [];
  private annoncesUpdated = new Subject<{annonces: Annonce[]; annonceCount: number}>();

  constructor(private http: HttpClient, private router: Router) { }

  getAnnonces(annoncesPerPage: number, currentPage: number) {
    const queryParams = `?pagesize=${annoncesPerPage}&page=${currentPage}`;
    this.http
      .get<{ message: string; annonces: any; maxAnnonces: number }>(
        BACKEND_URL + queryParams
      )
      .pipe(
        map(annonceData => {
          return {
            annonces: annonceData.annonces.map(annonce => {
              return {
                title: annonce.title,
                description: annonce.description,
                id: annonce._id,
                image: annonce.image,
                creator: annonce.creator
              };
            }),
            maxAnnonces: annonceData.maxAnnonces
          };
        })
      )
      .subscribe(transformedAnnonceData => {
        this.annonces = transformedAnnonceData.annonces;
        this.annoncesUpdated.next({
          annonces: [...this.annonces],
          annonceCount: transformedAnnonceData.maxAnnonces
        });
      });
  }

  getAnnonceUpdateListener() {
    return this.annoncesUpdated.asObservable();
  }

  getAnnonce(id: string) {
    return this.http.get<{
      _id: string;
      title: string;
      description: string;
      image: string;
      creator: string;
    }>(BACKEND_URL + id);
  }

  addAnnonce(title: string, description: string, image: File) {
    const annonceData = new FormData();
    annonceData.append("title", title);
    annonceData.append("description", description);
    annonceData.append("image", image, title);
    this.http
      .post<{ message: string; annonce: Annonce }>(
        BACKEND_URL,
        annonceData
      )
      .subscribe(responseData => {
        this.router.navigate(["/"]);
      });
  }

  updateAnnonce(id: string, title: string, description: string, image: File | string) {
    let annonceData: Annonce | FormData;
    if (typeof image === "object") {
      annonceData = new FormData();
      annonceData.append("id", id);
      annonceData.append("title", title);
      annonceData.append("description", description);
      annonceData.append("image", image, title);
    } else {
      annonceData = {
        id: id,
        title: title,
        description: description,
        image: image,
        creator: null
      };
    }
    this.http
      .put(BACKEND_URL + id, annonceData)
      .subscribe(response => {
        this.router.navigate(["/"]);
      });
  }

  deleteAnnonce(annonceId: string) {
    return this.http.delete(BACKEND_URL + annonceId);
  }
}
