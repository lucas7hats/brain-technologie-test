Bienvenue sur l'application de publication d'annonce.

Comment exécuter le projet?

I- Backend

1- Se positionner à la racine du dossier /backend.
2- Exécuter la commande "npm install" pour installer les dépendances.
3- Exécuter la commande "npm start" pour le lancer.


II- frontend

1- Se positionner à la racine du dossier /frontend.
2- Exécuter la commande "npm install" pour installer les dépendances.
3- Exécuter la commande "ng serve" pour le démarrer.
