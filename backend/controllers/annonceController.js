const Annonce = require("../schemas/annonceSchema");

exports.createAnnonce = (req, res, next) => {
  const url = req.protocol + "://" + req.get("host"); 
  const annonce = new Annonce({
    title: req.body.title,
    description: req.body.description,
    image: url + "/images/" + req.file.filename,
    creator: req.userData.userId
  });
  annonce
    .save()
    .then(createdAnnonce => {
      res.status(201).json({
        message: "Annonce créee avec succès",
        annonce: {
          ...createdAnnonce,
          id: createdAnnonce._id
        }
      });
    })
    .catch(error => {
      res.status(500).json({
        message: "Création échouée!"
      });
    });
};

exports.updateAnnonce = (req, res, next) => {
  let image = req.body.image;
  if (req.file) {
    const url = req.protocol + "://" + req.get("host");
    image = url + "/images/" + req.file.filename;
  }
  const annonce = new Annonce({
    _id: req.body.id,
    title: req.body.title,
    description: req.body.description,
    image: image,
    creator: req.userData.userId
  });
  Annonce.updateOne({ _id: req.params.id,  creator: req.userData.userId }, annonce)
    .then(result => {
      if (result.modifiedCount > 0) {
        res.status(200).json({ message: "Annonce modifiée avec succès!" });
      } else {
        res.status(401).json({ message: "Non autorisé" });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: "Modification échouée!"
      });
    });
};

exports.getAnnonces = (req, res, next) => {
  const pageSize = +req.query.pagesize;
  const currentPage = +req.query.page;
  const annonceQuery = Annonce.find();
  let fetchedAnnonces;
  if (pageSize && currentPage) {
    annonceQuery.skip(pageSize * (currentPage - 1)).limit(pageSize);
  }
  annonceQuery
    .then(documents => {
      fetchedAnnonces = documents;
      return Annonce.count();
    })
    .then(count => {
      res.status(200).json({
        message: "Annonces affichées avec succès!",
        annonces: fetchedAnnonces,
        maxAnnonces: count
      });
    })
    .catch(error => {
      res.status(500).json({
        message: "Affichage des données échoués!"
      });
    });
};

exports.getAnnonce = (req, res, next) => {
  Annonce.findById(req.params.id)
    .then(annonce => {
      if (annonce) {
        res.status(200).json(annonce);
      } else {
        res.status(404).json({ message: "Annonce not found!" });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: "Fetching annonce failed!"
      });
    });
};

exports.deleteAnnonce = (req, res, next) => {
  Annonce.deleteOne({ _id: req.params.id, creator: req.userData.userId})
    .then(result => {
      if (result.deletedCount > 0) {
        res.status(200).json({ message: "Supprimer avec succès!" });
      } else {
        res.status(401).json({ message: "Non autorisé!" });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: "Deleting annonces failed!"
      });
    });
};
