const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const annonceRoutes = require("./routes/annonceRoute");
const userRoutes = require("./routes/userRoute");

const dotenv = require('dotenv');

dotenv.config({ path: './config.env' });

const app = express();

mongoose
.connect(
    process.env.DB_URL, {
        useNewUrlParser: true,
       // useUnifiedTopology: true
      }
  )
  .then(() => {
    console.log("Connected to database!");
    console.log("App listen on address " + process.env.HOST +':'+ process.env.PORT );
  })
  .catch(() => {
    console.log("Connection failed!");
  });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/images", express.static(path.join("images")));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  next();
});

app.use("/api/annonces", annonceRoutes);
app.use("/api/user", userRoutes);

module.exports = app;
