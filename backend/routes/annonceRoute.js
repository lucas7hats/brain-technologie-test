const express = require("express");

const AnnonceController = require("../controllers/annonceController");

const checkAuth = require("../middlewares/check-auth");

const extractFile = require("../middlewares/file");

const router = express.Router();

router.post("", checkAuth, extractFile, AnnonceController.createAnnonce);

router.put("/:id", checkAuth, extractFile, AnnonceController.updateAnnonce);

router.get("", AnnonceController.getAnnonces);

router.get("/:id", AnnonceController.getAnnonce);

router.delete("/:id", checkAuth, AnnonceController.deleteAnnonce);

module.exports = router;
